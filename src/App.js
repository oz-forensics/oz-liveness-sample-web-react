import logo from './logo.svg';
import './App.css';

const handleOpenDemo = () => {
    window.OzLiveness.open({
        on_error: result => console.error('on_error', result),
        on_submit: result => console.log('on_submit', result),
        on_result: result => console.log('on_result', result),
        on_complete: result => console.log('on_complete', result),
        on_close: result => console.log('on_close', result),
        on_capture_complete: result => console.log('on_capture_complete', result),
    });
}

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>OzLiveness + React Demo</p>
      </header>

      <button onClick={handleOpenDemo}>Open Demo</button>
    </div>
  );
}

export default App;
